# mathematical-optimization

This repository contains different researches in the field of mathematical optimization, as well as solutions for tasks from the appropriate course.

---

In order to run existing scripts you may need to install some extra packages for python3.

You can simply do that by running the following commands:

```
pip3 install jupyter
pip3 install plotly
pip3 install numpy
```